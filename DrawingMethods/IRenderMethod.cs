﻿using System.Drawing;
using System.Windows.Forms;

namespace MultiDrawing.DrawingMethods
{
    public interface IRenderMethod : IRenderMethodActions
    {
        void Init(Form form);
        void PreDraw(PreDrawArgs args);
        void PostDraw(Form form);
        void CleanUp(Form form);
        void Update();
    }

    public class PreDrawArgs
    {
        public Form Form;
        public Color ClearColor;
    }
}
