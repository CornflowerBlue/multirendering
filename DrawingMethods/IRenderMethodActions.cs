﻿using System.Drawing;

namespace MultiDrawing.DrawingMethods
{
    public interface IRenderMethodActions
    {
        void DrawLine(float x, float y, float x2, float y2, Color color, float width);
        void DrawString(int x, int y, string text, Font font, Color color);
    }
}