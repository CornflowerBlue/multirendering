﻿using System.Collections.Generic;
using System.Windows.Forms;
using SharpDX;
using SharpDX.Direct3D9;
using Color = System.Drawing.Color;
using Font = SharpDX.Direct3D9.Font;

// You have to download SharpDX yourself and add the references to it yourself.
namespace MultiDrawing.DrawingMethods
{
    public class DirectXRendering : IRenderMethod
    {
        private Device _device;
        private PresentParameters _presentParameters;
        private Line _line;

        public Dictionary<int, Font> FontCache = new Dictionary<int, Font>(); 

        public void DrawLine(float x, float y, float x2, float y2, Color color, float width)
        {
            _line.Width = width;
            _line.Draw(new [] { new Vector2(x, y), new Vector2(x2, y2) }, new ColorBGRA(color.R, color.G, color.B, color.A));
        }

        public void DrawString(int x, int y, string text, System.Drawing.Font font, Color color)
        {
            // Since creating lots of fonts causes lag with DX, we cache them
            if (!FontCache.ContainsKey((int)font.Size))
                FontCache.Add((int)font.Size, new Font(_device, font));
            FontCache[(int)font.Size].DrawText(null, text, x, y, new ColorBGRA(color.R, color.G, color.B, color.A));
        }

        public void Init(Form form)
        {
            _presentParameters = new PresentParameters
            {
                Windowed = new Bool(true),
                SwapEffect = SwapEffect.Copy,
                BackBufferWidth = form.Width,
                BackBufferHeight = form.Height,
                BackBufferFormat = Format.A8R8G8B8,
                PresentationInterval = PresentInterval.Immediate
            };

            _device = new Device(new Direct3D(), 0, DeviceType.Hardware, form.Handle,
                CreateFlags.HardwareVertexProcessing, _presentParameters);

            _line = new Line(_device);
        }

        public void PreDraw(PreDrawArgs args)
        {
            _device.Clear(ClearFlags.Target, new SharpDX.Color(args.ClearColor.R, args.ClearColor.G, args.ClearColor.B, args.ClearColor.A), 1.0f, 0);

            _device.BeginScene();
        }

        public void PostDraw(Form form)
        {
            _device.EndScene();
            _device.Present();
        }

        public void CleanUp(Form form)
        {
            _device.Dispose();
        }

        public void Update()
        {

        }
    }
}
