﻿using System.Drawing;
using System.Windows.Forms;

namespace MultiDrawing.DrawingMethods
{
    public class CpuRendering : IRenderMethod
    {
        private BufferedGraphicsContext _graphicsContext;
        private BufferedGraphics _graphics;

        public void Init(Form form)
        {
            _graphicsContext = BufferedGraphicsManager.Current;
            _graphicsContext.MaximumBuffer = new Size(form.Width + 1, form.Height + 1);
            _graphics = _graphicsContext.Allocate(form.CreateGraphics(), form.ClientRectangle);
        }

        public void PreDraw(PreDrawArgs args)
        {
            _graphics.Graphics.Clear(args.ClearColor);
        }

        public void PostDraw(Form form)
        {
            _graphics.Render(); 
        }

        public void CleanUp(Form form)
        {
            _graphics.Dispose();
            _graphicsContext.Dispose();
        }

        public void Update()
        {
            
        }

        public void DrawLine(float x, float y, float x2, float y2, Color color, float width)
        {
            _graphics.Graphics.DrawLine(new Pen(color, width), x, y, x2, y2);
        }

        public void DrawString(int x, int y, string text, Font font, Color color)
        {
            _graphics.Graphics.DrawString(text, font, new SolidBrush(color), x, y);
        }
    }
}
