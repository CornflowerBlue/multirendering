﻿using System;
using System.Drawing;
using MultiDrawing.DrawingMethods;

namespace MultiDrawing
{
    public class RenderingSettings
    {
        internal IRenderMethod RenderingMethod { get; private set; }
        public bool DisplayFps { get; set; }

        /// <summary>
        /// The clear background color
        /// </summary>
        public Color ClearColor = Color.CornflowerBlue;

        /// <summary>
        /// Wait time between draw calls
        /// 50 = 20fps
        /// </summary>
        public int UpdateSpeed = 50;

        /// <summary>
        /// Use a custom form as the drawing form
        /// </summary>
        public Type CustomForm;

        public int Width = 720, Height = 480;

        public RenderingSettings(IRenderMethod renderingMethod)
        {
            RenderingMethod = renderingMethod;
        }
    }
}
