﻿using System;
using System.Diagnostics;
using System.Drawing;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using MultiDrawing.DrawingMethods;

namespace MultiDrawing
{
    public class DrawingForm :IDisposable
    {
        public event EventHandler<IRenderMethodActions> Draw;
        public event EventHandler<Form> InitializeForm; 

        private CancellationToken _cancellationToken;
        public readonly Form RenderingForm;

        internal IRenderMethod RenderingMethod { get; set; }
        internal RenderingSettings Settings { get; set; }

        public IntPtr Handle { get; private set; }

        // Fps Stuff
        private DateTime _lastUpdate;
        private int _currentFrames;
        public int CurrentFps { get; private set; }

        public DrawingForm(RenderingSettings settings)
        {
            RenderingMethod = settings.RenderingMethod;
            Settings = settings;

            if (settings.CustomForm != null)
            {
                if(settings.CustomForm.BaseType == typeof (Form))
                {
                    RenderingForm = Activator.CreateInstance(settings.CustomForm) as Form;
                }
                else
                {
                    throw new InvalidOperationException("Type of CustomForm is not type of Form");
                }
            }
            else
            {
                RenderingForm = new DoubleBufferedForm();
            }

            Handle = RenderingForm.Handle;
        }

        public void Start()
        {
            RenderingForm.Width = Settings.Width;
            RenderingForm.Height = Settings.Height;

            OnInitializeForm();

            RenderingForm.Show();
            RenderingMethod.Init(RenderingForm);

            _cancellationToken = new CancellationToken();
            Task.Factory.StartNew(GameLoop, _cancellationToken);

            Application.Run(RenderingForm);
        }

        private void GameLoop()
        {
            _lastUpdate = DateTime.Now;

            while (!_cancellationToken.IsCancellationRequested)
            {
                var sw = Stopwatch.StartNew();

                RenderingMethod.PreDraw(new PreDrawArgs { ClearColor = Settings.ClearColor, Form = RenderingForm });
                
                OnDraw(RenderingMethod);

                if(Settings.DisplayFps)
                    RenderingMethod.DrawString(0, 0, "Current FPS: " + CurrentFps, SystemFonts.DefaultFont, Color.IndianRed);

                RenderingMethod.PostDraw(RenderingForm);

                // update fps
                _currentFrames++;
                if (DateTime.Now.Subtract(_lastUpdate).TotalSeconds >= 1)
                {
                    _lastUpdate = DateTime.Now;
                    CurrentFps = _currentFrames;
                    _currentFrames = 0;
                }

                sw.Stop();

                var sleeptime = Settings.UpdateSpeed - (int) sw.ElapsedMilliseconds;

                if(sleeptime > 0)
                    Task.Delay(sleeptime, _cancellationToken).Wait(_cancellationToken);
            }
        }

        private void OnDraw(IRenderMethodActions e)
        {
            var handler = Draw;
            if (handler != null)
            {
                try
                {
                    handler.Invoke(this, e);
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex);
                }
            }
        }

        private void OnInitializeForm()
        {
            var handler = InitializeForm;
            if (handler != null) handler(this, RenderingForm);
        }

        public void Dispose()
        {
            RenderingMethod.CleanUp(RenderingForm);
            RenderingForm.Close();
            RenderingForm.Dispose();
        }
    }
}
