# Example Usage #

```
#!c#

[STAThread]
static void Main(string[] args)
{
    Application.EnableVisualStyles();
    Application.SetCompatibleTextRenderingDefault(false);

    var settings = new RenderingSettings(new CpuRendering())
    {
        UpdateSpeed = 16
    };

    using (var drawing = new DrawingForm(settings))
    {
        drawing.Draw += Draw;

        drawing.Start();
    }
}

private static void Draw(object sender, IRenderMethodActions a)
{
    a.DrawLine(0, 0, new Random().Next(0, 100), 100, Color.Red, 4);
    a.DrawString(10, 10, "hello", SystemFonts.DefaultFont, Color.Blue);
}
```