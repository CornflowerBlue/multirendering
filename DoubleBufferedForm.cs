﻿using System.Windows.Forms;

namespace MultiDrawing
{
    public partial class DoubleBufferedForm : Form
    {
        public DoubleBufferedForm()
        {
            InitializeComponent();

            SetStyle(ControlStyles.OptimizedDoubleBuffer |
                     ControlStyles.UserPaint |
                     ControlStyles.AllPaintingInWmPaint | 
                     ControlStyles.DoubleBuffer, true);
        }

        private void DoubleBufferedForm_Load(object sender, System.EventArgs e)
        {
            DoubleBuffered = true;
        }
    }
}
